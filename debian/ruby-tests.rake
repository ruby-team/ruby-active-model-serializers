require 'gem2deb/rake/testtask'

Gem2Deb::Rake::TestTask.new do |t|
  t.libs << 'lib' << 'test'
  t.test_files = FileList['test/**/*_test.rb'] - FileList['test/active_model_serializers/test/schema_test.rb', 'test/action_controller/json_api/pagination_test.rb', 'test/adapter/json_api/pagination_links_test.rb', 'test/grape_test.rb']
end
